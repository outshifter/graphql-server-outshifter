import { Module } from '@nestjs/common';
import { CategoriesService } from './categories.service';
import { CategoryResolver } from './categories.resolver';

@Module({
    providers: [CategoriesService, CategoryResolver]
})

export class CategoriesModule {}