import { Injectable } from "@nestjs/common";
import { Category } from "./entities/cateogorie.entities";
import { plainToClass } from 'class-transformer';
import  categoriesJson from './categories.json';
import { paginate } from '../common/pagination/paginate';
import { GetCategoriesArgs } from './dto/get-categories.args';

import Fuse from 'fuse.js';

const categories = plainToClass(Category, categoriesJson);
const options = {
  keys: ['name', 'type.slug'],
  threshold: 0.3,
};
const fuse = new Fuse(categories, options);
@Injectable()
export class CategoriesService {
    private categories: Category[] = categories;
    
    
    
    
    getCategory(): Category {
      return {
          name: "hola",
          description: "hola"
      }
        
    }
    
getCategories({ text, first, page, hasType, parent }: GetCategoriesArgs) {
  const startIndex = (page - 1) * first;
  const endIndex = page * first;
  let data: Category[] = this.categories;
  if (text?.replace(/%/g, '')) {
      data = fuse.search(text)?.map(({ item }) => item);
  }
  if (hasType?.value) {
    data = fuse.search(hasType.value as unknown)?.map(({ item }) => item);
  }
  if (parent === null) {
    data = data.filter(({ parent: parentValue }) => parentValue === null);
  }
  const results = data.slice(startIndex, endIndex);
  return {
    data: results,
    paginatorInfo: paginate(data.length, page, first, results.length),
    };
  }
}