import { ArgsType, Field, ID, InputType, ObjectType, registerEnumType } from "@nestjs/graphql";
import { Category } from "../entities/cateogorie.entities";
import { PaginatorInfo } from '../../common/dto/paginator-info.model';
import { PaginationArgs } from '../../common/dto/pagination.args';
import { WhereHasConditions, WhereHasConditionsRelation, SortOrder } from '../../common/dto/generic-conditions.input';
@ObjectType({ isAbstract: true })
export class CategoryPaginator {
  @Field(() => [Category])
  data: Category[];

  @Field(() => [PaginatorInfo])
  paginatorInfo: PaginatorInfo;
}

@ArgsType()
export class GetCategoriesArgs extends PaginationArgs {
  orderBy?: QueryCategoriesOrderByOrderByClause[];
  text?: string;
  hasType?: QueryCategoriesHasTypeWhereHasConditions;
  name?: string;
  @Field(() => ID, { nullable: true })
  parent?: number = null;
}

@InputType()
export class QueryCategoriesOrderByOrderByClause {
  column: QueryCategoriesOrderByColumn;
  order: SortOrder;
}
@InputType()
export class QueryCategoriesHasTypeWhereHasConditions extends WhereHasConditions {
  column: QueryCategoriesHasTypeColumn;
  AND?: QueryCategoriesHasTypeWhereHasConditions[];
  OR?: QueryCategoriesHasTypeWhereHasConditions[];
  HAS?: QueryCategoriesHasTypeWhereHasConditionsRelation;
}
@InputType()
export class QueryCategoriesHasTypeWhereHasConditionsRelation extends WhereHasConditionsRelation {
  condition: QueryCategoriesHasTypeWhereHasConditions;
}

export enum QueryCategoriesOrderByColumn {
  CREATED_AT = 'CREATED_AT',
  NAME = 'NAME',
  UPDATED_AT = 'UPDATED_AT',
}

registerEnumType(QueryCategoriesOrderByColumn, {
  name: 'QueryCategoriesOrderByColumn',
});

export enum QueryCategoriesHasTypeColumn {
  SLUG = 'SLUG',
}

registerEnumType(QueryCategoriesHasTypeColumn, {
  name: 'QueryCategoriesHasTypeColumn',
});
