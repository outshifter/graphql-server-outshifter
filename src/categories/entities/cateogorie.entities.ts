import { ObjectType, Field, Int, ID, InputType } from "@nestjs/graphql";
//import { CoreEntity } from "../../common/entities/core.entity";

@InputType({ isAbstract: true })
@ObjectType()
export class Category {
    @Field()
    name: string;
    @Field({nullable: true})
    description: string;
    parent?: Category[]
}

