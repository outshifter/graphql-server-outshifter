import { Resolver, Query, Mutation, Args, ID } from '@nestjs/graphql';
import { CategoriesService } from './categories.service';
import { Category } from './entities/cateogorie.entities';
import {
  CategoryPaginator,
  GetCategoriesArgs,
} from './dto/get-categories.args';
@Resolver(() => Category)
export class CategoryResolver {
    constructor(private readonly categoriesService: CategoriesService) { }
    
    @Query(() => Category, {name: 'Category'})
    async getCategory() {
        return this.categoriesService.getCategory()
    }

    @Query(() => CategoryPaginator, { name: 'Categories' })
    async getCategories(
        @Args() getCategoriesArgs: GetCategoriesArgs,
    ): Promise<CategoryPaginator>{
        return this.categoriesService.getCategories(getCategoriesArgs);
    }
}
