import { Module } from '@nestjs/common';
import { GraphQLModule } from '@nestjs/graphql';
import { ProductsModule } from './products/products.module';
import { CategoriesModule } from './categories/categories.module';
import { TestModule } from './test/test.module';
import { OrderModule } from './orders/orders.module'
import { ConfigModule } from '@nestjs/config';
import { CustomerModule } from './customer/customer.module';
import { ApolloServerPluginLandingPageLocalDefault } from 'apollo-server-core';


@Module({
    imports: [
    ConfigModule.forRoot(),
    GraphQLModule.forRoot({
      autoSchemaFile: true,
      playground: {
        endpoint:
          process.env.IS_NOT_SLS === 'true'
            ? '/graphql'
            : `/${process.env.STAGE}/graphql`,
      },
    }),
    ProductsModule,
    CategoriesModule,
    TestModule,
    OrderModule,
    CustomerModule
    ]
})

export class AppModule {}