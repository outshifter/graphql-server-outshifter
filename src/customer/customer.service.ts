import { Injectable, Logger } from '@nestjs/common';
import { Customer } from './entities/customer.entity';
import { plainToClass } from 'class-transformer';
import testJson from './test.json';
import { GetCustomerArgs, PagintationOrder } from './dto/get-customers.args';

const testdata = plainToClass(Customer, testJson);

@Injectable()
export class CustomerService {
  private tests: Partial<Customer>[] = testdata
  private info: Customer[] = testdata
    
  async getTest({ id }: GetCustomerArgs): Promise<Customer> {
    if (id) {
      return this.info.find((p) => p.id === Number(id));
    }
  }

  async findMany() {
    return this.tests
  }
  
}