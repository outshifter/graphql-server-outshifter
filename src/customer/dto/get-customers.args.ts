import { ArgsType, Field, Int, ObjectType } from "@nestjs/graphql";
import { CoreGetArguments } from "src/common/dto/core-get-arguments.args";
import { Customer } from "../entities/customer.entity";
@ArgsType()
export class GetCustomerArgs extends CoreGetArguments{ }

@ArgsType()
export class PagintationArgsTest {
    @Field((type) => Int)
    offset: number = 0;

    @Field((type) => Int)
    limit: number = 10;

    data: Customer[]
}
@ObjectType()
export class PagintationOrder {
    @Field((type) => Int)
    offset: number = 0;

    @Field((type) => Int)
    limit: number = 10;

    data: Customer[]
}

