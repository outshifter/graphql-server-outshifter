import { Resolver, Query, Args, ResolveField} from '@nestjs/graphql';
import { GetCustomerArgs } from './dto/get-customers.args';
import { Customer } from './entities/customer.entity';
import { CustomerService } from './customer.service';

@Resolver(() => Customer)
export class CustomerResolver {
  constructor(private readonly testService: CustomerService) { }

  @Query(() => Customer, {name: 'Customers'})
  async getCustomers(@Args() getTestsArgs: GetCustomerArgs,): Promise<Customer>{
    
    return this.testService.getTest(getTestsArgs);
  }

  @Query(() => [Customer])
  async tests() {
    return this.testService.findMany()
  }
/* 
  @Query(() => PagintationArgsTest, { name: 'getTests' })
  async getAllTests(@Args() getPaginationArgsTest: PagintationArgsTest): Promise<PagintationArgsTest> {
    return this.testService.getAllTests(getPaginationArgsTest);
  } */



}