import {
  ObjectType,
  Field,
  Int,
  registerEnumType,
  ID,
  InputType,
} from '@nestjs/graphql';
import { CoreEntity } from '../../common/entities/core.entity';

@ObjectType()
export class Customer {
  @Field(() => ID)
  id: number;
  @Field({ nullable: true })
  name?: string;
  @Field()
  description: string;
  
}
