import { Resolver, Query, Args, ResolveField} from '@nestjs/graphql';
import { GetTestsArgs, PagintationArgsTest } from './dto/get-test.args';
import { Test } from './entities/test.entity';
import { TestService } from './test.service';

@Resolver(() => Test)
export class TestResolver {
  constructor(private readonly testService: TestService) { }

  @Query(() => Test, {name: 'GetTests'})
  async getTest(@Args() getTestsArgs: GetTestsArgs,): Promise<Test>{
    
    return this.testService.getTest(getTestsArgs);
  }

  @Query(() => [Test], {name: 'GetaTest'})
  async tests() {
    return this.testService.findMany()
  }
/* 
  @Query(() => PagintationArgsTest, { name: 'getTests' })
  async getAllTests(@Args() getPaginationArgsTest: PagintationArgsTest): Promise<PagintationArgsTest> {
    return this.testService.getAllTests(getPaginationArgsTest);
  } */



}