import { Injectable, Logger } from '@nestjs/common';
import { Test } from './entities/test.entity';
import { plainToClass } from 'class-transformer';
import testJson from './test.json';
import { GetTestsArgs, PagintationTest } from './dto/get-test.args';

const testdata = plainToClass(Test, testJson);
console.log(testdata)
@Injectable()
export class TestService {
  private tests: Partial<Test>[] = testdata
  private info: Test[] = testdata
    
  async getTest({ id }: GetTestsArgs): Promise<Test> {
    if (id) {
      return this.info.find((p) => p.id === Number(id));
    }
  }

  async findMany() {
    return this.tests
  }
  
}