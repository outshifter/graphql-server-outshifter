import { ArgsType, Field, Int, ObjectType } from "@nestjs/graphql";
import { CoreGetArguments } from "src/common/dto/core-get-arguments.args";
import { Test } from "../entities/test.entity";
@ArgsType()
export class GetTestsArgs extends CoreGetArguments{ }

@ArgsType()
export class PagintationArgsTest {
    @Field((type) => Int)
    offset: number = 0;

    @Field((type) => Int)
    limit: number = 10;

    data: Test[]
}
@ObjectType()
export class PagintationTest {
    @Field((type) => Int)
    offset: number = 0;

    @Field((type) => Int)
    limit: number = 10;

    data: Test[]
}

