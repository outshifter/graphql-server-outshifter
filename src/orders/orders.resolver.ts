import { Resolver, Query, Args, ResolveField} from '@nestjs/graphql';
import { GetTestsArgs } from './dto/get-order.args';
import { Order } from './entities/order.entity';
import { OrdersService } from './orders.service';

@Resolver(() => Order)
export class OrderResolver {
  constructor(private readonly testService: OrdersService) { }

  @Query(() => Order, {name: 'Order'})
  async getTest(@Args() getTestsArgs: GetTestsArgs,): Promise<Order>{
    
    return this.testService.getTest(getTestsArgs);
  }

  @Query(() => [Order])
  async tests() {
    return this.testService.findMany()
  }
/* 
  @Query(() => PagintationArgsTest, { name: 'getTests' })
  async getAllTests(@Args() getPaginationArgsTest: PagintationArgsTest): Promise<PagintationArgsTest> {
    return this.testService.getAllTests(getPaginationArgsTest);
  } */



}