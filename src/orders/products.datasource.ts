import { RESTDataSource } from 'apollo-datasource-rest';
import { Injectable } from '@nestjs/common'

@Injectable()
class MociesAPI extends RESTDataSource {
    constructor(){
    super();
        this.baseURL = 'https://movies-api.example.com/';
    }
    async getMovie(id) {
        return this.get(`movies/${encodeURIComponent(id)}`);
    }

    async getMostViewedMovies(limit = 10) {
    const data = await this.get('movies', {
      per_page: limit,
      order_by: 'most_viewed',
    });
    return data.results;
  }
}
