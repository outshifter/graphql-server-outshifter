import { Injectable, Logger } from '@nestjs/common';
import { Order } from './entities/order.entity';
import { plainToClass } from 'class-transformer';
import testJson from './test.json';
import { GetTestsArgs, PagintationOrder } from './dto/get-order.args';

const testdata = plainToClass(Order, testJson);

@Injectable()
export class OrdersService {
  private tests: Partial<Order>[] = testdata
  private info: Order[] = testdata
    
  async getTest({ id }: GetTestsArgs): Promise<Order> {
    if (id) {
      return this.info.find((p) => p.id === Number(id));
    }
  }

  async findMany() {
    return this.tests
  }
  
}