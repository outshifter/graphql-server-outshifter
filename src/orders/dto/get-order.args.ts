import { ArgsType, Field, Int, ObjectType } from "@nestjs/graphql";
import { CoreGetArguments } from "src/common/dto/core-get-arguments.args";
import { Order } from "../entities/order.entity";
@ArgsType()
export class GetTestsArgs extends CoreGetArguments{ }

@ArgsType()
export class PagintationArgsTest {
    @Field((type) => Int)
    offset: number = 0;

    @Field((type) => Int)
    limit: number = 10;

    data: Order[]
}
@ObjectType()
export class PagintationOrder {
    @Field((type) => Int)
    offset: number = 0;

    @Field((type) => Int)
    limit: number = 10;

    data: Order[]
}

