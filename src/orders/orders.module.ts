import { Module } from '@nestjs/common';
import { OrdersService } from './orders.service';
import { OrderResolver } from './orders.resolver';

@Module({
  providers: [OrdersService, OrderResolver],
})
export class OrderModule {}
