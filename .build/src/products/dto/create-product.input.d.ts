import { Product } from '../entities/product.entity';
declare const CreateProductInput_base: import("@nestjs/common").Type<Omit<Product, "id" | "created_at" | "updated_at" | "slug">>;
export declare class CreateProductInput extends CreateProductInput_base {
}
export {};
