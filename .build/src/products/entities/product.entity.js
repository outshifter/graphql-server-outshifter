"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.VariationOption = exports.VariationInput = exports.Variation = exports.OrderProductPivot = exports.Product = void 0;
const graphql_1 = require("@nestjs/graphql");
const core_entity_1 = require("../../common/entities/core.entity");
var ProductStatus;
(function (ProductStatus) {
    ProductStatus["PUBLISH"] = "publish";
    ProductStatus["DRAFT"] = "draft";
})(ProductStatus || (ProductStatus = {}));
var ProductType;
(function (ProductType) {
    ProductType["SIMPLE"] = "simple";
    ProductType["VARIABLE"] = "variable";
})(ProductType || (ProductType = {}));
(0, graphql_1.registerEnumType)(ProductStatus, { name: 'ProductStatus' });
(0, graphql_1.registerEnumType)(ProductType, { name: 'ProductType' });
let Product = class Product extends core_entity_1.CoreEntity {
};
__decorate([
    (0, graphql_1.Field)({ nullable: true }),
    __metadata("design:type", String)
], Product.prototype, "name", void 0);
__decorate([
    (0, graphql_1.Field)(),
    __metadata("design:type", String)
], Product.prototype, "slug", void 0);
__decorate([
    (0, graphql_1.Field)(() => graphql_1.Int),
    __metadata("design:type", Number)
], Product.prototype, "type_id", void 0);
__decorate([
    (0, graphql_1.Field)(() => graphql_1.Int),
    __metadata("design:type", String)
], Product.prototype, "description", void 0);
__decorate([
    (0, graphql_1.Field)(),
    __metadata("design:type", Number)
], Product.prototype, "price", void 0);
__decorate([
    (0, graphql_1.Field)(() => graphql_1.Int),
    __metadata("design:type", Number)
], Product.prototype, "quantity", void 0);
Product = __decorate([
    (0, graphql_1.InputType)('ProductInputType', { isAbstract: true }),
    (0, graphql_1.ObjectType)()
], Product);
exports.Product = Product;
let OrderProductPivot = class OrderProductPivot {
};
__decorate([
    (0, graphql_1.Field)(() => graphql_1.ID),
    __metadata("design:type", Number)
], OrderProductPivot.prototype, "variation_option_id", void 0);
__decorate([
    (0, graphql_1.Field)(() => graphql_1.Int),
    __metadata("design:type", Number)
], OrderProductPivot.prototype, "order_quantity", void 0);
OrderProductPivot = __decorate([
    (0, graphql_1.InputType)('PivotInputType', { isAbstract: true }),
    (0, graphql_1.ObjectType)()
], OrderProductPivot);
exports.OrderProductPivot = OrderProductPivot;
let Variation = class Variation {
};
__decorate([
    (0, graphql_1.Field)(() => graphql_1.ID),
    __metadata("design:type", Number)
], Variation.prototype, "id", void 0);
__decorate([
    (0, graphql_1.Field)(() => graphql_1.Int),
    __metadata("design:type", Number)
], Variation.prototype, "quantity", void 0);
Variation = __decorate([
    (0, graphql_1.InputType)('VariationInputType', { isAbstract: true }),
    (0, graphql_1.ObjectType)()
], Variation);
exports.Variation = Variation;
let VariationInput = class VariationInput extends Variation {
};
VariationInput = __decorate([
    (0, graphql_1.InputType)()
], VariationInput);
exports.VariationInput = VariationInput;
let VariationOption = class VariationOption {
};
VariationOption = __decorate([
    (0, graphql_1.InputType)('VariationOptionInputType', { isAbstract: true }),
    (0, graphql_1.ObjectType)()
], VariationOption);
exports.VariationOption = VariationOption;
//# sourceMappingURL=product.entity.js.map