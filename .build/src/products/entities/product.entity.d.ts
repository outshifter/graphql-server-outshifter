import { CoreEntity } from '../../common/entities/core.entity';
declare enum ProductStatus {
    PUBLISH = "publish",
    DRAFT = "draft"
}
declare enum ProductType {
    SIMPLE = "simple",
    VARIABLE = "variable"
}
export declare class Product extends CoreEntity {
    name?: string;
    slug: string;
    type_id: number;
    product_type: ProductType;
    description: string;
    in_stock: boolean;
    is_taxable: boolean;
    sale_price?: number;
    max_price?: number;
    min_price?: number;
    sku?: string;
    status: ProductStatus;
    height?: string;
    length?: string;
    width?: string;
    price?: number;
    quantity: number;
    unit: string;
}
export declare class OrderProductPivot {
    variation_option_id?: number;
    order_quantity: number;
    unit_price: number;
    subtotal: number;
}
export declare class Variation {
    id: number;
    title: string;
    price: number;
    sku: string;
    is_disable: boolean;
    sale_price?: number;
    quantity: number;
    options: VariationOption[];
}
export declare class VariationInput extends Variation {
}
export declare class VariationOption {
    name: string;
    value: string;
}
export {};
