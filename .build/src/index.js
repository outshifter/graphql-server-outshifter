"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.handler = void 0;
const common_1 = require("@nestjs/common");
const core_1 = require("@nestjs/core");
const platform_express_1 = require("@nestjs/platform-express");
const serverless_express_1 = __importDefault(require("@vendia/serverless-express"));
const express_1 = __importDefault(require("express"));
const app_module_1 = require("./app.module");
let cachedServer;
const bootstrapServer = async () => {
    const expressApp = (0, express_1.default)();
    const app = await core_1.NestFactory.create(app_module_1.AppModule, new platform_express_1.ExpressAdapter(expressApp));
    app.useGlobalPipes(new common_1.ValidationPipe({ forbidUnknownValues: true }));
    app.enableCors();
    await app.init();
    return (0, serverless_express_1.default)({
        app: expressApp,
    });
};
const handler = async (event, context, callback) => {
    if (!cachedServer) {
        cachedServer = await bootstrapServer();
    }
    return cachedServer(event, context, callback);
};
exports.handler = handler;
//# sourceMappingURL=index.js.map