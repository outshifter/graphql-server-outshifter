"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.PagintationTest = exports.PagintationArgsTest = exports.GetTestsArgs = void 0;
const graphql_1 = require("@nestjs/graphql");
const core_get_arguments_args_1 = require("src/common/dto/core-get-arguments.args");
let GetTestsArgs = class GetTestsArgs extends core_get_arguments_args_1.CoreGetArguments {
};
GetTestsArgs = __decorate([
    (0, graphql_1.ArgsType)()
], GetTestsArgs);
exports.GetTestsArgs = GetTestsArgs;
let PagintationArgsTest = class PagintationArgsTest {
    constructor() {
        this.offset = 0;
        this.limit = 10;
    }
};
__decorate([
    (0, graphql_1.Field)((type) => graphql_1.Int),
    __metadata("design:type", Number)
], PagintationArgsTest.prototype, "offset", void 0);
__decorate([
    (0, graphql_1.Field)((type) => graphql_1.Int),
    __metadata("design:type", Number)
], PagintationArgsTest.prototype, "limit", void 0);
PagintationArgsTest = __decorate([
    (0, graphql_1.ArgsType)()
], PagintationArgsTest);
exports.PagintationArgsTest = PagintationArgsTest;
let PagintationTest = class PagintationTest {
    constructor() {
        this.offset = 0;
        this.limit = 10;
    }
};
__decorate([
    (0, graphql_1.Field)((type) => graphql_1.Int),
    __metadata("design:type", Number)
], PagintationTest.prototype, "offset", void 0);
__decorate([
    (0, graphql_1.Field)((type) => graphql_1.Int),
    __metadata("design:type", Number)
], PagintationTest.prototype, "limit", void 0);
PagintationTest = __decorate([
    (0, graphql_1.ObjectType)()
], PagintationTest);
exports.PagintationTest = PagintationTest;
//# sourceMappingURL=get-test.args.js.map