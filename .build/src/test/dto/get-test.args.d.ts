import { CoreGetArguments } from "src/common/dto/core-get-arguments.args";
import { Test } from "../entities/test.entity";
export declare class GetTestsArgs extends CoreGetArguments {
}
export declare class PagintationArgsTest {
    offset: number;
    limit: number;
    data: Test[];
}
export declare class PagintationTest {
    offset: number;
    limit: number;
    data: Test[];
}
