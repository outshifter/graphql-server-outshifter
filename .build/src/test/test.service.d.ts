import { Test } from './entities/test.entity';
import { GetTestsArgs } from './dto/get-test.args';
export declare class TestService {
    private tests;
    private info;
    getTest({ id }: GetTestsArgs): Promise<Test>;
    findMany(): Promise<Partial<Test>[]>;
}
