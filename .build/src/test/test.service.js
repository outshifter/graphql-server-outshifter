"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.TestService = void 0;
const common_1 = require("@nestjs/common");
const test_entity_1 = require("./entities/test.entity");
const class_transformer_1 = require("class-transformer");
const test_json_1 = __importDefault(require("./test.json"));
const testdata = (0, class_transformer_1.plainToClass)(test_entity_1.Test, test_json_1.default);
console.log(testdata);
let TestService = class TestService {
    constructor() {
        this.tests = testdata;
        this.info = testdata;
    }
    async getTest({ id }) {
        if (id) {
            return this.info.find((p) => p.id === Number(id));
        }
    }
    async findMany() {
        return this.tests;
    }
};
TestService = __decorate([
    (0, common_1.Injectable)()
], TestService);
exports.TestService = TestService;
//# sourceMappingURL=test.service.js.map