import { GetTestsArgs } from './dto/get-test.args';
import { Test } from './entities/test.entity';
import { TestService } from './test.service';
export declare class TestResolver {
    private readonly testService;
    constructor(testService: TestService);
    getTest(getTestsArgs: GetTestsArgs): Promise<Test>;
    tests(): Promise<Partial<Test>[]>;
}
