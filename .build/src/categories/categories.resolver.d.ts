import { CategoriesService } from './categories.service';
import { Category } from './entities/cateogorie.entities';
import { CategoryPaginator, GetCategoriesArgs } from './dto/get-categories.args';
export declare class CategoryResolver {
    private readonly categoriesService;
    constructor(categoriesService: CategoriesService);
    getCategory(): Promise<Category>;
    getCategories(getCategoriesArgs: GetCategoriesArgs): Promise<CategoryPaginator>;
}
