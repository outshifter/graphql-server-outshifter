import { Category } from "../entities/cateogorie.entities";
import { PaginatorInfo } from '../../common/dto/paginator-info.model';
import { PaginationArgs } from '../../common/dto/pagination.args';
import { WhereHasConditions, WhereHasConditionsRelation, SortOrder } from '../../common/dto/generic-conditions.input';
export declare class CategoryPaginator {
    data: Category[];
    paginatorInfo: PaginatorInfo;
}
export declare class GetCategoriesArgs extends PaginationArgs {
    orderBy?: QueryCategoriesOrderByOrderByClause[];
    text?: string;
    hasType?: QueryCategoriesHasTypeWhereHasConditions;
    name?: string;
    parent?: number;
}
export declare class QueryCategoriesOrderByOrderByClause {
    column: QueryCategoriesOrderByColumn;
    order: SortOrder;
}
export declare class QueryCategoriesHasTypeWhereHasConditions extends WhereHasConditions {
    column: QueryCategoriesHasTypeColumn;
    AND?: QueryCategoriesHasTypeWhereHasConditions[];
    OR?: QueryCategoriesHasTypeWhereHasConditions[];
    HAS?: QueryCategoriesHasTypeWhereHasConditionsRelation;
}
export declare class QueryCategoriesHasTypeWhereHasConditionsRelation extends WhereHasConditionsRelation {
    condition: QueryCategoriesHasTypeWhereHasConditions;
}
export declare enum QueryCategoriesOrderByColumn {
    CREATED_AT = "CREATED_AT",
    NAME = "NAME",
    UPDATED_AT = "UPDATED_AT"
}
export declare enum QueryCategoriesHasTypeColumn {
    SLUG = "SLUG"
}
