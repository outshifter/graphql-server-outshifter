"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.QueryCategoriesHasTypeColumn = exports.QueryCategoriesOrderByColumn = exports.QueryCategoriesHasTypeWhereHasConditionsRelation = exports.QueryCategoriesHasTypeWhereHasConditions = exports.QueryCategoriesOrderByOrderByClause = exports.GetCategoriesArgs = exports.CategoryPaginator = void 0;
const graphql_1 = require("@nestjs/graphql");
const cateogorie_entities_1 = require("../entities/cateogorie.entities");
const paginator_info_model_1 = require("../../common/dto/paginator-info.model");
const pagination_args_1 = require("../../common/dto/pagination.args");
const generic_conditions_input_1 = require("../../common/dto/generic-conditions.input");
let CategoryPaginator = class CategoryPaginator {
};
__decorate([
    (0, graphql_1.Field)(() => [cateogorie_entities_1.Category]),
    __metadata("design:type", Array)
], CategoryPaginator.prototype, "data", void 0);
__decorate([
    (0, graphql_1.Field)(() => [paginator_info_model_1.PaginatorInfo]),
    __metadata("design:type", paginator_info_model_1.PaginatorInfo)
], CategoryPaginator.prototype, "paginatorInfo", void 0);
CategoryPaginator = __decorate([
    (0, graphql_1.ObjectType)({ isAbstract: true })
], CategoryPaginator);
exports.CategoryPaginator = CategoryPaginator;
let GetCategoriesArgs = class GetCategoriesArgs extends pagination_args_1.PaginationArgs {
    constructor() {
        super(...arguments);
        this.parent = null;
    }
};
__decorate([
    (0, graphql_1.Field)(() => graphql_1.ID, { nullable: true }),
    __metadata("design:type", Number)
], GetCategoriesArgs.prototype, "parent", void 0);
GetCategoriesArgs = __decorate([
    (0, graphql_1.ArgsType)()
], GetCategoriesArgs);
exports.GetCategoriesArgs = GetCategoriesArgs;
let QueryCategoriesOrderByOrderByClause = class QueryCategoriesOrderByOrderByClause {
};
QueryCategoriesOrderByOrderByClause = __decorate([
    (0, graphql_1.InputType)()
], QueryCategoriesOrderByOrderByClause);
exports.QueryCategoriesOrderByOrderByClause = QueryCategoriesOrderByOrderByClause;
let QueryCategoriesHasTypeWhereHasConditions = class QueryCategoriesHasTypeWhereHasConditions extends generic_conditions_input_1.WhereHasConditions {
};
QueryCategoriesHasTypeWhereHasConditions = __decorate([
    (0, graphql_1.InputType)()
], QueryCategoriesHasTypeWhereHasConditions);
exports.QueryCategoriesHasTypeWhereHasConditions = QueryCategoriesHasTypeWhereHasConditions;
let QueryCategoriesHasTypeWhereHasConditionsRelation = class QueryCategoriesHasTypeWhereHasConditionsRelation extends generic_conditions_input_1.WhereHasConditionsRelation {
};
QueryCategoriesHasTypeWhereHasConditionsRelation = __decorate([
    (0, graphql_1.InputType)()
], QueryCategoriesHasTypeWhereHasConditionsRelation);
exports.QueryCategoriesHasTypeWhereHasConditionsRelation = QueryCategoriesHasTypeWhereHasConditionsRelation;
var QueryCategoriesOrderByColumn;
(function (QueryCategoriesOrderByColumn) {
    QueryCategoriesOrderByColumn["CREATED_AT"] = "CREATED_AT";
    QueryCategoriesOrderByColumn["NAME"] = "NAME";
    QueryCategoriesOrderByColumn["UPDATED_AT"] = "UPDATED_AT";
})(QueryCategoriesOrderByColumn = exports.QueryCategoriesOrderByColumn || (exports.QueryCategoriesOrderByColumn = {}));
(0, graphql_1.registerEnumType)(QueryCategoriesOrderByColumn, {
    name: 'QueryCategoriesOrderByColumn',
});
var QueryCategoriesHasTypeColumn;
(function (QueryCategoriesHasTypeColumn) {
    QueryCategoriesHasTypeColumn["SLUG"] = "SLUG";
})(QueryCategoriesHasTypeColumn = exports.QueryCategoriesHasTypeColumn || (exports.QueryCategoriesHasTypeColumn = {}));
(0, graphql_1.registerEnumType)(QueryCategoriesHasTypeColumn, {
    name: 'QueryCategoriesHasTypeColumn',
});
//# sourceMappingURL=get-categories.args.js.map