export declare class Category {
    name: string;
    description: string;
    parent?: Category[];
}
