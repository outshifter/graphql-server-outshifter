import { Category } from "./entities/cateogorie.entities";
import { GetCategoriesArgs } from './dto/get-categories.args';
export declare class CategoriesService {
    private categories;
    getCategory(): Category;
    getCategories({ text, first, page, hasType, parent }: GetCategoriesArgs): {
        data: Category[];
        paginatorInfo: import("../common/dto/paginator-info.model").PaginatorInfo;
    };
}
